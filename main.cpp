#include <iostream>
#include <vector>
#include <map>
#include <list>
#include <fstream>

using namespace std;

vector<bool> code;
map<char, vector<bool>> table;

class Node;
void print(Node* root, unsigned k  = 0);

class Node
{
public:
    int a;
    char c;
    Node* left;
    Node* right;

    Node(){}

    Node(Node* L, Node* R)
    {
        left = L;
        right = R;
        a = L->a + R->a;
    }
};

void buildTable(Node* root)
{
    if(root->left != NULL)
    {
        code.push_back(0);
        buildTable(root->left);
    }
    if(root->right != NULL)
    {
        code.push_back(1);
        buildTable(root->right);
    }
    if(root->c)
        table[root->c] = code;
    if(!code.empty())
        code.pop_back();
}

struct MyCompare
{
    bool operator()(Node* l, Node* r) const
    {
        return l->a < r->a;
    }
};

int main()
{

    map<char, int> m;

//    string s = "it is my striiiiing!!!!";
//    for(int i = 0; i < s.length(); i++)
//    {
//        char c = s[i];
//        m[c]++;
//    }
    ifstream f("1.txt");

    while(!f.eof())
    {
        char c;
        //f>>c;
        c = f.get();
        m[c]++;
    }

    list<Node*> t;
    map<char, int>::iterator i;
    for (i = m.begin(); i != m.end(); ++i)
    {
        Node *p = new Node;
        p->c = i -> first;
        p->a = i -> second;
        t.push_back(p);
    }
    while (t.size() != 1)
    {
        t.sort(MyCompare());

        Node* SonL = t.front();
        t.pop_front();
        Node *SonR = t.front();
        t.pop_front();

        Node* parent = new Node(SonL, SonR);
        t.push_back(parent);
    }

    Node* root = t.front();

    buildTable(root);
//    for(int i = 0; i < s.length(); i++)
//    {
//        char c = s[i];
//        vector<bool> x = table[c];

//        for (int n = 0; n < x.size(); n++)
//        {
//            cout << x[n];
//        }
//    }
    f.clear();
    f.seekg(0);

    ofstream g("output.bin");
    int count = 0; char buf = 0;

    while (!f.eof())
    {
        char c;
        //f >> c;
        c = f.get();
        vector<bool> x = table[c];

        for (int n = 0; n < x.size(); n++)
        {
            cout << x[n];
            buf = buf | x[n] << (7 - count);
            count++;
            if(count == 8)
            {
                count = 0;
                g << buf;
                buf = 0;
            }
        }
    }
    cout << endl;

    g.close();
    f.close();

    ifstream F("output.bin");
    Node *p = root;
    count = 0;
    char byte;
//    F>>byte;
    byte = F.get();
    while(!F.eof())
    {
        bool b = byte & (1 << (7 - count));
        if(b)
            p = p->right;
        else
            p = p->left;
        if(p->left == NULL && p->right == NULL)
        {
            cout << p->c;
            p=root;
        }
        count++;
        if(count == 8)
        {
            count = 0;
            //F>>byte;
            byte = F.get();
        }
    }

    F.close();
    return 0;
}

void print(Node* root, unsigned k)
{
    if(root != NULL)
    {
        print(root->left, k + 3);
        for(unsigned i = 0; i < k; i++)
        {
            cout << "  ";
        }

        if(root->c)
            cout << root->a << " (" << root->c<<")"<<endl;
        else
            cout << root->a << endl;
        print(root->right, k + 3);
    }
}
