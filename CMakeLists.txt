cmake_minimum_required(VERSION 3.5)

project(Huffman_cpp LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(Huffman_cpp main.cpp)
